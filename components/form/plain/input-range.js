import { Template } from 'meteor/templating';
import SimpleSchema from 'simpl-schema';

// import template
import './input-range.html';

Template.inputRange.helpers({
  attributes() {
    const atts = { ...this };

    delete atts.onChange;
    delete atts.min;
    delete atts.max;
    atts.class = `form-control ${atts.class}` || '';
    return atts;
  },
});

Template.inputRange.events({
  'change input'(e) {
    let value = parseFloat(e.currentTarget.value);
    const min = this.min === 0 ? this.min : this.min || false;
    const max = this.max || false;

    if (min !== false) {
      if (isNaN(value) || value < min) {
        e.currentTarget.value = min;
        value = min;
      }
    }
    if (max !== false) {
      if (value > max) {
        e.currentTarget.value = max;
        value = max;
      }
    }
    if (this.onChange) {
      this.onChange(value);
    }
  },
});

Template.inputRange.onCreated(function () {
  this.autorun(() => {
    new SimpleSchema({
      name: { type: String },
      class: { type: String, optional: true },
      id: { type: String },
      step: { type: Number, optional: true },
      min: { type: Number, optional: true },
      max: { type: Number, optional: true },
            // для пустой строки
      value: { type: SimpleSchema.oneOf(String, Number), optional: true },
      onChange: { type: Function, optional: true },
    }).validate(Template.currentData());
  });
});

Template.inputRange.onRendered(function () {

});

Template.inputRange.onDestroyed(function () {

});

