import { TemplateController } from 'meteor/template-controller';
import SimpleSchema from 'simpl-schema';
import c from 'classnames';
import { Match } from 'meteor/check';
import { Random } from 'meteor/random';

// import template
import './input-checkbox.html';

TemplateController('InputCheckbox', {
  // Validate the properties passed to the template from parents
  props: new SimpleSchema({
    name: { type: String },
    className: { type: String, optional: true },
    label: { type: String, optional: true },
    id: {
      type: String,
      autoValue() {
        if (!this.isSet) {
          return Random.id();
        }
      },
    },
    checked: { type: Boolean, defaultValue: false },
    disabled: { type: Boolean, defaultValue: false },
    inline: { type: Boolean, defaultValue: false },
    circle: { type: Boolean, defaultValue: false },
    onChange: { type: Function, optional: true },
    // булевое значение превращается в строку 'true'
    value: { type: SimpleSchema.oneOf(String, Boolean), defaultValue: true },
    color: {
      type: String, defaultValue: 'primary', allowedValues: ['default', 'primary', 'custom'],
    },
    colorCustom: { type: String, optional: true },
  }),

  // Setup private reactive template state
  state: {},

  // Lifecycle callbacks work exactly like with standard Blaze
  onCreated() {
  },
  onRendered() {
  },
  onDestroyed() {
  },

  // Helpers work like before but <this> is always the template instance!
  helpers: {
    attributes() {
      const {
        name, id, value, checked, disabled,
      } = this.props;
      return {
        name,
        id,
        value,
        checked,
        disabled,
      };
    },
    className() {
      const {
        className, color, inline, circle,
      } = this.props;
      return c({
        [this.prefix]: true,
        [className]: !!className,
        [`${this.prefix}-${color}`]: !!color,
        [`${this.prefix}-inline`]: inline,
        [`${this.prefix}-circle`]: circle,
      });
    },
    customColor() {
      return this.props.color === 'custom';
    },
  },

  // Events work like before but <this> is always the template instance!
  events: {
    'change input'(e) {
      if (this.props.onChange) {
        e.preventDefault();
        // для одиночных инпутов булевских полей
        if (this.props.value === 'true' || Match.test(this.props.value, Boolean)) {
          this.props.onChange(e.currentTarget.checked, e.currentTarget);
        } else {
          this.props.onChange(e.currentTarget.value, e.currentTarget);
        }
      }
    },
  },

  // These are added to the template instance but not exposed to the html
  private: {
    prefix: 'checkbox',
  },
});
