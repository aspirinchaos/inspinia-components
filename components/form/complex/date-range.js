import { TemplateController } from 'meteor/template-controller';
import SimpleSchema from 'simpl-schema';

// import components
import '../../lib/datepicker';
import '../plain/input-text';
import '../plain/input-date';

// import template
import './date-range.html';

const dateScheme = new SimpleSchema({
  name: { type: String },
  value: { type: String, optional: true },
  params: { type: Object, defaultValue: {}, blackbox: true },
});

TemplateController('dateRange', {
  // Validate the properties passed to the template from parents
  props: new SimpleSchema({
    id: { type: String, optional: true },
    label: { type: String, optional: true },
    className: { type: String, optional: true },
    onChange: { type: Function, optional: true },
    clear: { type: Boolean, defaultValue: false },
    start: { type: dateScheme },
    end: { type: dateScheme },
    format: { type: String, defaultValue: 'dd.mm.yyyy' },
  }),

  // Setup private reactive template state
  state: {},

  // Lifecycle callbacks work exactly like with standard Blaze
  onCreated() {
  },
  onRendered() {
    if (!Meteor.isCordova) {
      this.$('.input-daterange').datepicker({
        keyboardNavigation: false,
        forceParse: false,
        autoclose: true,
        language: 'ru',
        format: this.props.format,
        todayHighlight: true,
        clearBtn: this.props.clear,
      }).on('changeDate', (e) => {
        if (this.props.onChange) {
          this.props.onChange(e.target.name, e.date);
        }
      }).on('clearDate', (e) => {
        if (this.props.onChange) {
          this.props.onChange(e.target.name, '');
        }
      });
    }
  },
  onDestroyed() {
  },

  // Helpers work like before but <this> is always the template instance!
  helpers: {
    start() {
      const { name, value, params } = this.props.start;
      return { name, value, ...params };
    },
    end() {
      const { name, value, params } = this.props.end;
      return { name, value, ...params };
    },
    startCordova() {
      const { name, value, params } = this.props.start;
      return {
        name,
        value,
        ...params,
        onChange: date => this.props.onChange(name, date),
      };
    },
    endCordova() {
      const { name, value, params } = this.props.end;
      return {
        name,
        value,
        ...params,
        onChange: date => this.props.onChange(name, date),
      };
    },
    isCordova() {
      return Meteor.isCordova;
    },
  },

  // Events work like before but <this> is always the template instance!
  events: {},

  // These are added to the template instance but not exposed to the html
  private: {},
});
