import { TemplateController } from 'meteor/template-controller';
import SimpleSchema from 'simpl-schema';
import '../../lib/colorpicker';

// import components
import '../common/input-group';
import '../plain/input-text';

// import template
import './color-picker.html';

TemplateController('colorPicker', {
  // Validate the properties passed to the template from parents
  props: new SimpleSchema({
    name: { type: String },
    value: { type: String, optional: true },
    id: { type: String, optional: true },
    className: { type: String, optional: true },
  }),

  // Setup private reactive template state
  state: {},

  // Lifecycle callbacks work exactly like with standard Blaze
  onCreated() {
  },
  onRendered() {
    this.$('.colorpicker-component').colorpicker({
      color: this.props.value || false,
    });
  },
  onDestroyed() {
  },

  // Helpers work like before but <this> is always the template instance!
  helpers: {
    attributes() {
      const { name, value, id, className } = this.props;
      return { name, id, value, className };
    },
  },

  // Events work like before but <this> is always the template instance!
  events: {},

  // These are added to the template instance but not exposed to the html
  private: {},
});
