import { TemplateController } from 'meteor/template-controller';
import SimpleSchema from 'simpl-schema';

// import template
import './input-hidden.html';

TemplateController('inputHidden', {
  // Validate the properties passed to the template from parents
  props: new SimpleSchema({
    name: { type: String },
    value: { type: String, optional: true },
    onChange: { type: Function, optional: true },
  }),

  // Setup private reactive template state
  state: {},

  // Lifecycle callbacks work exactly like with standard Blaze
  onCreated() {
  },
  onRendered() {
  },
  onDestroyed() {
  },

  // Helpers work like before but <this> is always the template instance!
  helpers: {
    attributes() {
      const atts = { ...this.data };

      delete atts.onChange;
      return atts;
    },
  },

  // Events work like before but <this> is always the template instance!
  events: {
    'change input'(e) {
      e.preventDefault();
      if (this.props.onChange) {
        this.props.onChange(e.currentTarget.value);
      }
    },
  },

  // These are added to the template instance but not exposed to the html
  private: {},
});
