import { TemplateController } from 'meteor/template-controller';
import SimpleSchema from 'simpl-schema';

// import template
import './input-number.html';

TemplateController('inputNumber', {
  // Validate the properties passed to the template from parents
  props: new SimpleSchema({
    name: { type: String },
    className: { type: String, optional: true },
    id: { type: String },
    integer: { type: Boolean, optional: true },
    disabled: { type: Boolean, optional: true },
    step: { type: Number, optional: true },
    min: { type: Number, optional: true },
    max: { type: Number, optional: true },
    // для пустой строки
    value: { type: SimpleSchema.oneOf(String, Number), optional: true },
    onChange: { type: Function, optional: true },
  }),

  // Setup private reactive template state
  state: {},

  // Lifecycle callbacks work exactly like with standard Blaze
  onCreated() {
  },
  onRendered() {
  },
  onDestroyed() {
  },

  // Helpers work like before but <this> is always the template instance!
  helpers: {
    attributes() {
      const { name, id, className, step, min, max, disabled, value } = this.props;
      return {
        name,
        id,
        step,
        min,
        max,
        disabled,
        value,
        class: `${className || ''} form-control`,
      };
    },
  },

  // Events work like before but <this> is always the template instance!
  events: {
    'keypress input'(e) {
      // Allow: backspace, delete, tab, escape, enter and .
      if ([46, 8, 9, 27, 13, 110, 190].indexOf(e.keyCode) !== -1 ||
        // Allow: Ctrl+A, Command+A
        (e.keyCode === 65 && (e.ctrlKey === true || e.metaKey === true)) ||
        // Allow: home, end, left, right, down, up
        (e.keyCode >= 35 && e.keyCode <= 40)) {
        // let it happen, don't do anything
        return;
      }
      // Ensure that it is a number and stop the keypress
      if (((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) &&
        (e.keyCode < 96 || e.keyCode > 105)) || e.keyCode === 101) {
        e.preventDefault();
      }
    },
    'change input'(e) {
      let value = parseFloat(e.currentTarget.value);
      const min = this.props.min === 0 ? this.props.min : this.props.min || false;
      const max = this.props.max || false;

      if (this.props.integer) {
        value = parseInt(e.currentTarget.value, 10);
      }
      if (min !== false) {
        if (Number.isNaN(value) || value < min) {
          e.currentTarget.value = min;
          value = min;
        }
      }
      if (max !== false) {
        if (value > max) {
          e.currentTarget.value = max;
          value = max;
        }
      }
      if (this.props.onChange) {
        this.props.onChange(value);
      }
    },
  },

  // These are added to the template instance but not exposed to the html
  private: {},
});
