import { TemplateController } from 'meteor/template-controller';
import SimpleSchema from 'simpl-schema';
import 'jquery-sparkline';

// import template
import './sparkline.html';

TemplateController('sparkline', {
    // Validate the properties passed to the template from parents
    props: new SimpleSchema({
        // todd: доработать компонент
        values: { type: Array },
        'values.$': { type: Number },
        type: { type: String, defaultValue: 'line', allowedValues: [ 'line' ] },
    }),

    // Setup private reactive template state
    state: {},

    // Lifecycle callbacks work exactly like with standard Blaze
    onCreated() {
    },
    onRendered() {
        this.$('.sparkline').sparkline(this.props.values, {
            type: this.props.type,
            width: '100%',
            height: '50',
            lineColor: '#1ab394',
            fillColor: 'transparent',
        });
    },
    onDestroyed() {
    },

    // Helpers work like before but <this> is always the template instance!
    helpers: {},

    // Events work like before but <this> is always the template instance!
    events: {},

    // These are added to the template instance but not exposed to the html
    private: {},
});
