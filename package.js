Package.describe({
  name: 'inspinia-components',
  version: '0.2.4',
  documentation: 'README.md',
});

Npm.depends({
  // 'bootstrap-colorpicker': '2.5.2',
  'jquery-sparkline': '2.4.0',
  peity: '3.2.1',
  'chart.js': '2.7.1',
});

Package.onUse((api) => {
  api.versionsFrom('1.5');
  api.use([
    'ecmascript',
    'templating',
    'template-controller',
    'tmeasday:check-npm-versions',
  ]);
  api.mainModule('inspinia-components.js', 'client');
});
