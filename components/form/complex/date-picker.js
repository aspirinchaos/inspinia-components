import { TemplateController } from 'meteor/template-controller';
import moment from 'moment';
import SimpleSchema from 'simpl-schema';

// import components
import '../plain/input-text';
import '../../lib/datepicker';
import '../common/input-group';

// import template
import './date-picker.html';

TemplateController('datePicker', {
  props: new SimpleSchema({
    onChange: { type: Function, optional: true },
    name: { type: String, optional: true },
    id: { type: String, optional: true },
    value: { type: SimpleSchema.oneOf(String, Date), optional: true },
    startDate: { type: Date, optional: true },
    clear: { type: Boolean, defaultValue: false },
    className: { type: String, optional: true },
  }),

  // Setup private reactive template state
  state: {},

  // Lifecycle callbacks work exactly like with standard Blaze
  onCreated() {
  },
  onRendered() {
    this.$('.input-group.date').datepicker({
      keyboardNavigation: false,
      forceParse: false,
      autoclose: true,
      language: 'ru',
      format: 'dd.mm.yyyy',
      todayHighlight: true,
      startDate: this.props.startDate,
      defaultViewDate: this.props.value || 'today',
      clearBtn: this.props.clear,
    }).on('changeDate', (e) => {
      if (this.props.onChange) {
        this.props.onChange(e.date, e.format());
      }
    }).on('clearDate', (e) => {
      if (this.props.onChange) {
        this.props.onChange('', '');
      }
    });
  },
  onDestroyed() {
  },

  // Helpers work like before but <this> is always the template instance!
  helpers: {
    attributes() {
      const { name, id, value, className } = this.props;
      const date = (value && moment(new Date(value))) || '';
      return {
        name,
        className,
        id,
        datetype: true,
        value: (date && date.isValid() && date.format('DD.MM.YYYY')) || value,
      };
    },
  },

  // Events work like before but <this> is always the template instance!
  events: {},

  // These are added to the template instance but not exposed to the html
  private: {},
});
