import { TemplateController } from 'meteor/template-controller';
import SimpleSchema from 'simpl-schema';

// import components
import '../../common/button-dropdown';

// import template
import './input-group.html';

TemplateController('inputGroup', {
  // Validate the properties passed to the template from parents
  props: new SimpleSchema({
    type: { type: String, allowedValues: ['btn-affix', 'btn-prefix', 'affix', 'prefix', 'dropdown'] },
    text: { type: String, optional: true },
    className: { type: String, optional: true },
    template: { type: String, optional: true },
    data: { type: Object, blackbox: true, optional: true },
  }),

  // Setup private reactive template state
  state: {},

  // Lifecycle callbacks work exactly like with standard Blaze
  onCreated() {
  },
  onRendered() {
  },
  onDestroyed() {
  },

  // Helpers work like before but <this> is always the template instance!
  helpers: {
    isType(type) {
      return this.props.type === type;
    },
    dynamic() {
      const { template, data } = this.props;
      return {
        template,
        data,
      };
    },
  },

  // Events work like before but <this> is always the template instance!
  events: {},

  // These are added to the template instance but not exposed to the html
  private: {},
});
