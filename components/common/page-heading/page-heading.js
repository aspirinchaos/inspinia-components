import { TemplateController } from 'meteor/template-controller';
import SimpleSchema from 'simpl-schema';
import { FlowRouter } from 'meteor/kadira:flow-router';

import './page-heading.html';

TemplateController('pageHeading', {
  // Validate the properties passed to the template from parents
  props: new SimpleSchema({
    title: { type: String },
    breadcrumb: { type: Boolean, defaultValue: true },
    activeTitle: { type: String, optional: true },
    category: { type: String, optional: true },
    categoryRoute: { type: String, optional: true },
  }),

  // Setup private reactive template state
  state: {},

  // Lifecycle callbacks work exactly like with standard Blaze
  onCreated() {
  },
  onRendered() {
  },
  onDestroyed() {
  },

  // Helpers work like before but <this> is always the template instance!
  helpers: {
    route() {
      if (this.props.categoryRoute) {
        return FlowRouter.path(this.props.categoryRoute);
      }
      return '#';
    },
    activeTitle() {
      return this.props.activeTitle || this.props.title;
    },
  },

  // Events work like before but <this> is always the template instance!
  events: {},

  // These are added to the template instance but not exposed to the html
  private: {},
});
