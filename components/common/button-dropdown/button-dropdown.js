import { TemplateController } from 'meteor/template-controller';
import SimpleSchema from 'simpl-schema';
import classNames from 'classnames';

// import components
import './button-dropdown-element';

// import template
import './button-dropdown.html';

TemplateController('buttonDropdown', {
  // Validate the properties passed to the template from parents
  props: new SimpleSchema({
    className: { type: String, optional: true },
    text: { type: String },
    size: { type: String, optional: true },
    right: { type: Boolean, defaultValue: false },
    handleSelect: { type: Function, optional: true },
    options: { type: Array },
    'options.$': {
      type: Object,
      blackbox: true,
    },
    'options.$.value': {
      type: String,
      optional: true,
    },
    'options.$.label': {
      type: String,
    },
  }),

  // Setup private reactive template state
  state: {},

  // Lifecycle callbacks work exactly like with standard Blaze
  onCreated() {
  },
  onRendered() {
  },
  onDestroyed() {
  },

  // Helpers work like before but <this> is always the template instance!
  helpers: {
    button() {
      return {
        'data-toggle': 'dropdown',
        class: `btn btn-white dropdown-toggle ${this.props.size || ''}`,
        type: 'button',
        'aria-expanded': 'false',
      };
    },
    dropdownClass() {
      return classNames({
        'dropdown-menu': true,
        'dropdown-menu-right': this.props.right,
      });
    },
    divider(option) {
      return option.label === 'divider';
    },
    getOption(option) {
      return {
        ...option,
        handleSelect: this.props.handleSelect,
      };
    },
  },

  // Events work like before but <this> is always the template instance!
  events: {},

  // These are added to the template instance but not exposed to the html
  private: {},
});
