import { checkNpmVersions } from 'meteor/tmeasday:check-npm-versions';

import './components/common/button';
import './components/common/spinner';
import './components/common/ibox-tools';
//import './button-dropdown';
//import './page-heading';
//import './sparkline';
//import './typeahead';
//import './peity';
//import './chart';

import './components/form';
//import './components/common';
//import './components/data-table';

checkNpmVersions({
  'simpl-schema': '1.5.3',
  classnames: '2.2.6',
  moment: '2.22.2',
}, 'inspinia-components');
