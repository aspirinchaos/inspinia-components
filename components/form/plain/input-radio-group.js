import { Template } from 'meteor/templating';
import SimpleSchema from 'simpl-schema';

// import template
import './input-radio-group.html';

Template.inputRadioGroup.helpers({
  attributes(option, index) {
    const { name, id, value } = this;

    return {
      name,
      id: `${id}-${index}`,
      value: option.value,
      checked: option.value === value,
    };
  },
  forId(index) {
    return `${this.id}-${index}`;
  },
  class() {
    return this.class || 'radio-primary';
  },
});

Template.inputRadioGroup.events({
  'change input'(e, t) {
    e.preventDefault();
    if (t.data.onChange) {
      t.data.onChange(e.currentTarget.value);
    }
  },
});

Template.inputRadioGroup.onCreated(function () {
  this.autorun(() => {
    new SimpleSchema({
      name: { type: String },
      class: { type: String, optional: true },
      id: { type: String },
            // для пустой строки
      onChange: { type: Function, optional: true },
      value: { type: SimpleSchema.oneOf(String, Number), optional: true },
      options: { type: Array },
      'options.$': {
        type: new SimpleSchema({
          value: { type: String },
          label: { type: String },
        }),
      },
    }).validate(Template.currentData());
  });
});

Template.inputRadioGroup.onRendered(function () {

});

Template.inputRadioGroup.onDestroyed(function () {

});

