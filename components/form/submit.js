import { TemplateController } from 'meteor/template-controller';
import SimpleSchema from 'simpl-schema';
import classNames from 'classnames';

// import components
import '../common/button';

// import template
import './submit.html';

TemplateController('Submit', {
  // Validate the properties passed to the template from parents
  props: new SimpleSchema({
    horizontal: { type: Boolean, defaultValue: false },
    content: { type: String, defaultValue: 'Готово' },
    color: { type: String, defaultValue: 'primary' },
    className: { type: String, optional: true },
    size: {
      type: new SimpleSchema({
        lg: {
          type: Number, min: 1, max: 12, defaultValue: 10,
        },
        md: {
          type: Number, min: 1, max: 12, defaultValue: 9,
        },
        sm: {
          type: Number, optional: true, min: 1, max: 12,
        },
        xs: {
          type: Number, optional: true, min: 1, max: 12,
        },
      }),
      defaultValue: {},
    },
  }),

  // Setup private reactive template state
  state: {},

  // Lifecycle callbacks work exactly like with standard Blaze
  onCreated() {
  },
  onRendered() {
  },
  onDestroyed() {
  },

  // Helpers work like before but <this> is always the template instance!
  helpers: {
    size() {
      const {
        lg, md, sm, xs,
      } = this.props.size;
      return (this.props.horizontal && classNames({
        [`col-lg-${lg}`]: !!lg,
        [`col-md-${md}`]: !!md,
        [`col-sm-${sm}`]: !!sm,
        [`col-xs-${xs}`]: !!xs,
        [`col-lg-offset-${12 - lg}`]: !!lg,
        [`col-md-offset-${12 - md}`]: !!md,
        [`col-sm-offset-${12 - sm}`]: !!sm,
        [`col-xs-offset-${12 - xs}`]: !!xs,
      })) || '';
    },
    buttonData() {
      const { content, color, className } = this.props;
      return {
        type: 'submit',
        content,
        color,
        className,
      };
    },
  },

  // Events work like before but <this> is always the template instance!
  events: {},

  // These are added to the template instance but not exposed to the html
  private: {},
});
