import { TemplateController } from 'meteor/template-controller';
import SimpleSchema from 'simpl-schema';
import Chart from 'chart.js';

// import template
import './chart.html';

TemplateController('chart', {
  // Validate the properties passed to the template from parents
  props: new SimpleSchema({
    // todo@aspirin добавить больше проверок?
    type: { type: String },
    options: { type: Object, blackbox: true, defaultValue: {} },
    data: { type: Object, blackbox: true },
  }),

  // Setup private reactive template state
  state: {},

  // Lifecycle callbacks work exactly like with standard Blaze
  onCreated() {

  },
  onRendered() {
    const { type, data, options } = this.props;
    this.chart = new Chart(this.find('canvas').getContext('2d'), { type, data, options });
    this.autorun(() => {
      if (this.props.data.datasets.length) {
        this.chart.data = this.props.data;
        this.chart.update();
      }
    });
  },
  onDestroyed() {
  },

  // Helpers work like before but <this> is always the template instance!
  helpers: {},

  // Events work like before but <this> is always the template instance!
  events: {},

  // These are added to the template instance but not exposed to the html
  private: {},
});