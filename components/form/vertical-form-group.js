import { TemplateController } from 'meteor/template-controller';
import SimpleSchema from 'simpl-schema';
import classNames from 'classnames';

// import template
import './vertical-form-group.html';

TemplateController('VerticalFormGroup', {
  // Validate the properties passed to the template from parents
  props: new SimpleSchema({
    checkbox: { type: Boolean, defaultValue: false },
    hr: { type: Boolean, defaultValue: false },
    label: { type: SimpleSchema.oneOf(String, Boolean), optional: true },
    className: { type: String, optional: true },
    id: { type: String, optional: true },
    size: {
      type: new SimpleSchema({
        lg: { type: Number, optional: true, min: 1, max: 12 },
        md: { type: Number, optional: true, min: 1, max: 12 },
        sm: { type: Number, optional: true, min: 1, max: 12 },
        xs: { type: Number, optional: true, min: 1, max: 12 },
      }),
      defaultValue: {},
    },
    help: { type: String, optional: true },
  }),

  // Setup private reactive template state
  state: {},

  // Lifecycle callbacks work exactly like with standard Blaze
  onCreated() {
  },
  onRendered() {
  },
  onDestroyed() {
  },

  // Helpers work like before but <this> is always the template instance!
  helpers: {
    className() {
      const { className } = this.props;
      const { lg, md, sm, xs } = this.props.size;
      return classNames({
        'form-group': true,
        [`col-lg-${lg}`]: !!lg,
        [`col-md-${md}`]: !!md,
        [`col-sm-${sm}`]: !!sm,
        [`col-xs-${xs}`]: !!xs,
        [className]: !!className,
      });
    },
  },

  // Events work like before but <this> is always the template instance!
  events: {},

  // These are added to the template instance but not exposed to the html
  private: {},
});
