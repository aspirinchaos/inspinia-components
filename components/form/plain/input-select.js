import { TemplateController } from 'meteor/template-controller';
import SimpleSchema from 'simpl-schema';
import { check, Match } from 'meteor/check';

// import template
import './input-select.html';

TemplateController('inputSelect', {
  // Validate the properties passed to the template from parents
  props: new SimpleSchema({
    name: { type: String },
    className: { type: String, optional: true },
    id: { type: String, optional: true },
    // для пустой строки
    onChange: { type: Function, optional: true },
    value: {
      type: SimpleSchema.oneOf(String, Number),
      autoValue() {
        if (!this.isSet) {
          return '';
        }
        if (!Number.isNaN(Number(this.value))) {
          return Number(this.value);
        }
        return this.value;
      },
    },
    disabled: { type: Boolean, defaultValue: false },
    options: { type: Array },
    'options.$': {
      type: SimpleSchema.oneOf(new SimpleSchema({
        value: { type: SimpleSchema.oneOf(String, Number), defaultValue: '' },
        label: { type: SimpleSchema.oneOf(String, Number), defaultValue: '' },
        disabled: { type: Boolean, defaultValue: false },
      }), String, Number),
    },
  }),

  // Setup private reactive template state
  state: {},

  // Lifecycle callbacks work exactly like with standard Blaze
  onCreated() {
  },
  onRendered() {
  },
  onDestroyed() {
  },

  // Helpers work like before but <this> is always the template instance!
  helpers: {
    params(option) {
      return {
        value: option.value,
        selected: option.value === this.props.value,
        disabled: option.disabled,
      };
    },
    options() {
      this.options = this.props.options.map((option) => {
        if (Match.test(option, String) || Match.test(option, Number)) {
          return {
            value: option,
            label: option,
          };
        }
        return option;
      });
      return this.options;
    },
    attributes() {
      const { disabled, name, id } = this.props;

      return {
        disabled,
        id,
        name,
        class: this.className || 'form-control',
      };
    },
  },

  // Events work like before but <this> is always the template instance!
  events: {
    'change select'(e) {
      e.preventDefault();
      if (this.props.onChange) {
        const select = e.currentTarget;
        const option = this.options[select.selectedIndex];
        this.props.onChange(option.value, option.label, option);
      }
    },
  },

  // These are added to the template instance but not exposed to the html
  private: {
    options: [],
  },
});
