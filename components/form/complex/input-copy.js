import { TemplateController } from 'meteor/template-controller';
import SimpleSchema from 'simpl-schema';

// import components
import '../common/input-group';

// import template
import './input-copy.html';

TemplateController('inputCopy', {
  // Validate the properties passed to the template from parents
  props: new SimpleSchema({
    name: { type: String, optional: true },
    id: { type: String, optional: true },
    className: { type: String, optional: true },
    placeholder: { type: String, optional: true },
    value: { type: String, optional: true },
    readonly: { type: Boolean, defaultValue: true },
    onClick: { type: Function, optional: true },
  }),

  // Setup private reactive template state
  state: {},

  // Lifecycle callbacks work exactly like with standard Blaze
  onCreated() {
  },
  onRendered() {
  },
  onDestroyed() {
  },

  // Helpers work like before but <this> is always the template instance!
  helpers: {
    groupData() {
      return {
        type: 'btn-affix',
        className: this.props.groupClass,
        template: 'button',
        data: {
          color: 'primary',
          text: '<i class="fa fa-copy"></i>',
          onClick: () => {
            this.find('input').select();
            document.execCommand('Copy');
            if (this.props.onClick) {
              this.props.onClick();
            }
          },
        },
      };
    },
    attributes() {
      const {
        name, id, className, placeholder, value, readonly,
      } = this.props;
      return {
        id,
        name,
        placeholder,
        value,
        readonly,
        class: `${className || ''} form-control`,
      };
    },
  },

  // Events work like before but <this> is always the template instance!
  events: {},

  // These are added to the template instance but not exposed to the html
  private: {},
});
