import { TemplateController } from 'meteor/template-controller';
import SimpleSchema from 'simpl-schema';

// import components
import '../plain/input-text';
import '../plain/input-hidden';

// import template
import './double-input.html';

TemplateController('doubleInput', {
  // Validate the properties passed to the template from parents
  props: new SimpleSchema({
    name: { type: Array, minCount: 2, maxCount: 2 },
    'name.$': { type: String },
    id: { type: String, optional: true },
    separator: { type: String, defaultValue: '-' },
    className: { type: String, optional: true },
    placeholder: { type: String, optional: true },
    value: { type: Array, optional: true, maxCount: 2 },
    'value.$': { type: String, optional: true },
    onChange: { type: Function, optional: true },
    onInput: { type: Function, optional: true },
    disabled: { type: Boolean, optional: true },
    onlyNumbers: { type: Boolean, defaultValue: false },
  }),

  // Setup private reactive template state
  state: {
    value1: null,
    value2: null,
  },

  // Lifecycle callbacks work exactly like with standard Blaze
  onCreated() {
    if (this.props.value) {
      const [value1, value2] = this.props.value;
      this.state.value1 = value1;
      this.state.value2 = value2;
    }
  },
  onRendered() {
  },
  onDestroyed() {
  },

  // Helpers work like before but <this> is always the template instance!
  helpers: {
    input() {
      const {
        id, className, placeholder, onChange,
        onInput, value, separator, onlyNumbers: on,
      } = this.props;
      return {
        id,
        className,
        placeholder,
        value: value.join(separator),
        onChange: (val) => {
          const [value1, value2] = val.split(separator);
          this.state.value1 = value1;
          this.state.value2 = value2;
          if (onChange) {
            onChange(value1, value2);
          }
        },
        onInput: (val, target) => {
          if (on) {
            target.value = val.replace(new RegExp(`([^0-9\\${separator}])`, 'gi'), '');
          }
          const [value1, value2] = val.split(separator);
          this.state.value1 = value1;
          this.state.value2 = value2;
          if (onInput) {
            onInput(value1, value2);
          }
        },
        ...(() => (on && { pattern: `\\d+${separator}\\d+`, inputmode: 'numeric' }) || {})(),
      };
    },
    hidden1() {
      const { name } = this.props;
      return {
        name: name[0],
        value: this.state.value1,
      };
    },
    hidden2() {
      const { name } = this.props;
      return {
        name: name[1],
        value: this.state.value2,
      };
    },
  },

  // Events work like before but <this> is always the template instance!
  events: {},

  // These are added to the template instance but not exposed to the html
  private: {},
});
