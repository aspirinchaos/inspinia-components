import { TemplateController } from 'meteor/template-controller';
import SimpleSchema from 'simpl-schema';

import './ibox-tools.html';

TemplateController('IboxTools', {
  // Validate the properties passed to the template from parents
  props: new SimpleSchema({
    collapse: {
      type: new SimpleSchema({
        show: { type: Boolean, defaultValue: false },
        isCollapsed: { type: Boolean, defaultValue: false },
      }),
      optional: true,
    },
    dropdown: { type: Boolean, optional: true },
    close: { type: Boolean, optional: true },
    link: {
      type: new SimpleSchema({
        href: { type: String, defaultValue: '#' },
        text: { type: String },
        onClick: { type: Function, optional: true },
      }),
      optional: true,
    },
    // для задания контекста
    context: { type: Object, optional: true, blackbox: true },
  }),

  // Setup private reactive template state
  state: {},

  // Lifecycle callbacks work exactly like with standard Blaze
  onCreated() {
  },
  onRendered() {
    if (this.props.collapse && this.props.collapse.isCollapsed) {
      this.$('.ibox-tools').closest('div.ibox').addClass('collapsed');
    }
  },
  onDestroyed() {
  },

  // Helpers work like before but <this> is always the template instance!
  helpers: {},

  // Events work like before but <this> is always the template instance!
  events: {
    'click .collapse-link'(e) {
      const element = this.$(e.target);
      const ibox = element.closest('div.ibox');
      const button = element.closest('i');
      const content = ibox.find('div.ibox-content');

      content.slideToggle(200);
      button.toggleClass('fa-chevron-up').toggleClass('fa-chevron-down');
    },
    'click .close-link'(e) {
      const element = this.$(e.target);
      const content = element.closest('div.ibox');
      content.remove();
    },
    'click .js-link'(e) {
      if (this.props.link.onClick) {
        e.preventDefault();
        this.props.link.onClick(e);
      }
    },
  },

  // These are added to the template instance but not exposed to the html
  private: {},
});
