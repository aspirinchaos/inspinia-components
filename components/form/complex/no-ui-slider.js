import { Template } from 'meteor/templating';
import SimpleSchema from 'simpl-schema';
import Slider from 'nouislider';

// import template
import './no-ui-slider.html';

Template.noUiSlider.helpers({});

Template.noUiSlider.events({});

Template.noUiSlider.onCreated(function () {
  this.autorun(() => {
    new SimpleSchema({
      class: { type: String, optional: true },
      slider: { type: Object, blackbox: true },
      onChange: { type: Function, optional: true },
    }).validate(Template.currentData());
  });
});

Template.noUiSlider.onRendered(function () {
  const slider = this.find('.js-slider');
  Slider.create(this.find('.js-slider'), this.data.slider);

  if (this.data.onChange) {
    slider.noUiSlider.on('update', this.data.onChange);
  }
});

Template.noUiSlider.onDestroyed(function () {

});

