import { TemplateController } from 'meteor/template-controller';
import SimpleSchema from 'simpl-schema';
import { ReactiveDict } from 'meteor/reactive-dict';
import { Match } from 'meteor/check';

import './data-table-header';
import './data-table-search';
import './data-table-info';
import './data-table-limit';
import './data-table-paginate';
import './data-table.html';

TemplateController('dataTable', {
  // Validate the properties passed to the template from parents
  props: new SimpleSchema({
    state: { type: ReactiveDict, blackbox: true },
    schema: { type: Object, blackbox: true },
    list: { type: Function },
    columns: { type: Array },
    'columns.$': { type: SimpleSchema.oneOf(String, Object) },
    'columns.$.field': { type: String, defaultValue: '' },
    'columns.$.value': { type: String, optional: true },
    'columns.$.label': { type: String, defaultValue: '' },
    selectColumn: { type: Boolean, optional: true },
    extraColumn: { type: String, optional: true },
  }),

  // Setup private reactive template state
  state: {
    selected: [],
  },

  // Lifecycle callbacks work exactly like with standard Blaze
  onCreated() {
  },
  onRendered() {
  },
  onDestroyed() {
  },

  // Helpers work like before but <this> is always the template instance!
  helpers: {
    limit() {
      return {
        limit: this.data.state.get('limit'),
        onChange: (value) => {
          this.data.state.set('limit', value);
          this.data.state.set('skip', 0);
        },
      };
    },
    search() {
      return {
        search: this.data.state.get('search'),
        onChange: (value) => {
          if (value.length > 3) {
            this.data.state.set('search', value);
          } else {
            this.data.state.set('search', '');
          }
        },
      };
    },
    info() {
      const limit = this.data.state.get('limit');
      const skip = this.data.state.get('skip');
      const total = this.data.state.get('total');
      const selected = this.state.selected.length;
      return {
        limit, skip, total, selected,
      };
    },
    header() {
      const columns = this.props.columns.map((column) => {
        // field можно не указывать, если не нужна сортировка
        const key = (Match.test(column, String) && column) || column.field || column.label;
        return { key, label: this.label(column) };
      });
      const sortColumn = this.data.state.get('sortColumn');
      const sortDirection = this.data.state.get('sortDirection');

      return {
        columns,
        sortColumn,
        sortDirection,
        onChange: (value) => {
          if (value === sortColumn) {
            this.data.state.set('sortDirection', sortDirection * -1);
          } else {
            this.data.state.set('sortColumn', value);
          }
        },
        selectColumn: this.props.selectColumn,
        extraColumn: !!this.props.extraColumn,
        params: {
          id: 'select-all',
          name: 'data-table-select-all',
          onChange: (value) => {
            if (value) {
              this.state.selected = this.props.list().map(({ _id }) => _id);
            } else {
              this.state.selected = [];
            }
          },
        },
      };
    },
    evenOrOdd(index) {
      return (index + 1) % 2 ? 'even' : 'odd';
    },
    select(row) {
      const { selected } = this.state;
      return {
        name: 'data-table-select-row',
        id: row._id,
        checked: selected.indexOf(row._id) !== -1,
        onChange: (value) => {
          const index = selected.indexOf(row._id);
          if (value) {
            this.state.selected = [...selected, row._id];
          } else {
            selected.splice(index, 1);
            this.state.selected = [...selected];
          }
        },
      };
    },
    value(row, col) {
      if (Match.test(col, String)) {
        return row[col];
      }
      if (Match.test(row[col.value], Function)) {
        return row[col.value]();
      }
      return row[col.value];
    },
    extra(row) {
      return {
        row,
        selected: this.state.selected,
      };
    },
    colLength() {
      const { columns, selectColumn, extraColumn } = this.props;
      return columns.length + ((selectColumn && 1) || 0) + ((extraColumn && 1) || 0);
    },
    label(column) {
      return this.label(column);
    },
    paginate() {
      return {
        limit: this.data.state.get('limit'),
        skip: this.data.state.get('skip'),
        total: this.data.state.get('total'),
        onChange: (newSkip) => {
          this.data.state.set('skip', newSkip);
        },
      };
    },

  },

  // Events work like before but <this> is always the template instance!
  events: {},

  // These are added to the template instance but not exposed to the html
  private: {
    label(column) {
      if (typeof column !== 'object') {
        return this.props.schema.label(column) || '';
      }
      return column.label || this.props.schema.label(column.field) || '';
    },
  },
});
