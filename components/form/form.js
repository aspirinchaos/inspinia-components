import { TemplateController } from 'meteor/template-controller';
import SimpleSchema from 'simpl-schema';
import classNames from 'classnames';
import getFormData from '../../get-form-data';

// import components
import './submit';
import './input-list';

// import template
import './form.html';

TemplateController('Form', {
  // Validate the properties passed to the template from parents
  props: new SimpleSchema({
    horizontal: { type: Boolean, defaultValue: false },
    inline: { type: Boolean, defaultValue: false },
    hr: { type: Boolean, optional: true },
    name: { type: String },
    fields: { type: Array, optional: true },
    'fields.$': {
      type: SimpleSchema.oneOf(String,
        {
          type: new SimpleSchema({
            template: { type: String, optional: true },
            field: { type: SimpleSchema.oneOf(String, Array) },
            'field.$': { type: String, optional: true },
            params: { type: Object, blackbox: true, optional: true },
          }),
        }),
    },
    schema: {
      // после обновления слетели типы
      type: Object,
      blackbox: true,
      optional: true,
    },
    className: { type: String, optional: true },
    onSubmit: { type: Function, optional: true },
    handleForm: { type: Function, optional: true },
    doc: { type: Object, blackbox: true, optional: true },
    size: { type: Object, blackbox: true, optional: true },
    submit: { type: Object, blackbox: true, optional: true },
  }),

  // Setup private reactive template state
  state: {},

  // Lifecycle callbacks work exactly like with standard Blaze
  onCreated() {
  },
  onRendered() {
    // Вернем DOM узел формы и функцию для получения данных с формы
    if (this.props.handleForm) {
      const form = this.find('form');
      this.props.handleForm({ form, getData: () => getFormData(form) });
    }
  },
  onDestroyed() {
  },

  // Helpers work like before but <this> is always the template instance!
  helpers: {
    class() {
      const { className, horizontal, inline } = this.props;
      return classNames({
        'form-horizontal': horizontal,
        'form-inline': inline,
        [`${className}`]: !!className,
      });
    },
    listData() {
      const {
        name, fields, schema, doc, size, horizontal, hr,
      } = this.props;
      return {
        name, fields, schema, doc, size, horizontal, hr,
      };
    },
    submitData() {
      const { submit, size, horizontal } = this.props;
      return { size, horizontal, ...submit };
    },
    hasSubmit() {
      return !!this.props.onSubmit;
    },
  },

  // Events work like before but <this> is always the template instance!
  events: {
    'submit form'(e) {
      e.preventDefault();
      if (this.props.onSubmit) {
        this.props.onSubmit(e.target, getFormData(e.target));
      }
    },
  },

  // These are added to the template instance but not exposed to the html
  private: {},
});
