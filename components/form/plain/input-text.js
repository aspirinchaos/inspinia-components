import { TemplateController } from 'meteor/template-controller';
import SimpleSchema from 'simpl-schema';

// import template
import './input-text.html';

TemplateController('InputText', {
  // Validate the properties passed to the template from parents
  props: new SimpleSchema({
    name: { type: String, optional: true },
    id: { type: String, optional: true },
    className: { type: String, optional: true },
    placeholder: { type: String, optional: true },
    value: { type: String, optional: true },
    onChange: { type: Function, optional: true },
    onInput: { type: Function, optional: true },
    disabled: { type: Boolean, optional: true },
    datetype: { type: Boolean, defaultValue: false },
  }),

  // Setup private reactive template state
  state: {},

  // Lifecycle callbacks work exactly like with standard Blaze
  onCreated() {
  },
  onRendered() {
  },
  onDestroyed() {
  },

  // Helpers work like before but <this> is always the template instance!
  helpers: {
    attributes() {
      const atts = { ...this.data };

      delete atts.onChange;
      delete atts.onInput;
      delete atts.className;
      return {
        ...atts,
        class: `${this.props.className || ''} form-control`,
      };
    },
  },

  // Events work like before but <this> is always the template instance!
  events: {
    'change input'(e) {
      if (this.props.onChange) {
        e.preventDefault();
        this.props.onChange(e.currentTarget.value, e.currentTarget);
      }
    },
    'input input'(e) {
      if (this.props.onInput) {
        e.preventDefault();
        this.props.onInput(e.currentTarget.value, e.currentTarget);
      }
    },
  },

  // These are added to the template instance but not exposed to the html
  private: {},
});
