import { TemplateController } from 'meteor/template-controller';
import SimpleSchema from 'simpl-schema';
import { Tracker } from 'meteor/tracker';
import '../../lib/typeahead';

// import template
import './typeahead.html';

TemplateController('typeahead', {
  // Validate the properties passed to the template from parents
  props: new SimpleSchema({
    display: { type: String, optional: true },
    label: { type: String, optional: true },
    placeholder: { type: String, optional: true },
    name: { type: String, optional: true },
    handleSelect: { type: Function, optional: true },
    onInput: { type: Function, optional: true },
    loaded: { type: Boolean, optional: true },
    items: { type: Array, optional: true },
    'items.$': {
      type: new SimpleSchema({
        value: { type: String },
        label: { type: String, optional: true },
      }),
      optional: true,
    },
    limit: { type: SimpleSchema.Integer, defaultValue: 5 },
  }),

  // Setup private reactive template state
  state: {},

  // Lifecycle callbacks work exactly like with standard Blaze
  onCreated() {
  },
  onRendered() {
    this.autorun(() => {
      this.async(this.props.items);
    });
    const { display, limit } = this.props;
    this.input = this.$('input').typeahead(
      {
        hint: true,
        highlight: true,
        minLength: 1,
        dynamic: true,
      },
      {
        limit,
        display,
        source: (q, sync, async) => {
          this.async = async;
        },
        async: true,
      },
    ).bind('typeahead:select', (ev, suggestion) => {
      if (this.props.handleSelect) {
        this.input.typeahead('val', '');
        this.props.handleSelect(suggestion);
      }
    });
  },
  onDestroyed() {
  },

  // Helpers work like before but <this> is always the template instance!
  helpers: {},

  // Events work like before but <this> is always the template instance!
  events: {
    'input input'(e) {
      if (this.props.onInput) {
        e.preventDefault();
        this.props.onInput(e.currentTarget.value);
      }
    },
  },

  // These are added to the template instance but not exposed to the html
  private: {
    typeahead: null,
    async: () => {},
  },
});
