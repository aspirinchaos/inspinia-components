import { TemplateController } from 'meteor/template-controller';
import SimpleSchema from 'simpl-schema';

// import components
import '../plain/input-text';
import '../../lib/clockpicker';
import '../common/input-group';

// import template
import './clock-picker.html';

TemplateController('clockPicker', {
  // Validate the properties passed to the template from parents
  props: new SimpleSchema({
    onChange: { type: Function, optional: true },
    name: { type: String, optional: true },
    id: { type: String, optional: true },
    value: { type: String, optional: true },
    placeholder: { type: String, optional: true },
    className: { type: String, optional: true },
  }),

  // Setup private reactive template state
  state: {},

  // Lifecycle callbacks work exactly like with standard Blaze
  onCreated() {
  },
  onRendered() {
    this.$('.input-group.clock').clockpicker({
      autoclose: true,
    });
  },
  onDestroyed() {
  },

  // Helpers work like before but <this> is always the template instance!
  helpers: {
    attributes() {
      const { name, id, value, placeholder, onChange, className } = this.props;
      return {
        name,
        id,
        value,
        placeholder,
        onChange,
        className,
      };
    },
  },

  // Events work like before but <this> is always the template instance!
  events: {},

  // These are added to the template instance but not exposed to the html
  private: {},
});
