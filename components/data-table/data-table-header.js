import { TemplateController } from 'meteor/template-controller';
import SimpleSchema from 'simpl-schema';

import './data-table-header.html';

TemplateController('dataTableHeader', {
  // Validate the properties passed to the template from parents
  props: new SimpleSchema({
    columns: { type: Array },
    'columns.$': { type: Object },
    'columns.$.key': { type: String, defaultValue: '' },
    'columns.$.label': { type: String, defaultValue: '' },
    sortColumn: { type: String },
    sortDirection: { type: Number },
    onChange: { type: Function },
    selectColumn: { type: Boolean, optional: true },
    params: {
      type: new SimpleSchema({
        name: { type: String },
        id: { type: String },
        label: { type: String, optional: true },
        checked: { type: Boolean, optional: true },
        onChange: { type: Function, optional: true },
        // для пустой строки
        value: { type: String, optional: true },
      }),
      optional: true,
    },
    extraColumn: { type: Boolean, optional: true },
  }),

  // Setup private reactive template state
  state: {},

  // Lifecycle callbacks work exactly like with standard Blaze
  onCreated() {
  },
  onRendered() {
  },
  onDestroyed() {
  },

  // Helpers work like before but <this> is always the template instance!
  helpers: {
    columns() {
      return this.props.columns.map(({ key, label }) => ({
        label,
        sorting: this.sorting(key),
        onClick: () => {
          this.props.onChange(key);
        },
      }));
    },
  },

  // Events work like before but <this> is always the template instance!
  events: {},

  // These are added to the template instance but not exposed to the html
  private: {
    sorting(key) {
      if (!key) {
        return '';
      }
      const { sortColumn: sc, sortDirection: sd } = this.props;
      return (sc === key && ((sd === -1 && 'sorting_desc') || 'sorting_asc')) || 'sorting';
    },
  },
});

TemplateController('dataTableHeaderColumn', {
  props: new SimpleSchema({
    label: { type: String, defaultValue: '' },
    sorting: { type: String, defaultValue: '' },
    onClick: Function,
  }),
  events: {
    'click th'() {
      this.props.onClick();
    },
  },
});
