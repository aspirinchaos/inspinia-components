import { TemplateController } from 'meteor/template-controller';
import { _ } from 'meteor/underscore';
import SimpleSchema from 'simpl-schema';

import './data-table-paginate.html';

TemplateController('dataTablePaginateItem', {
  // Validate the properties passed to the template from parents
  props: new SimpleSchema({
    page: SimpleSchema.oneOf(Number, String),
    currentPage: Number,
    changePage: Function,
  }),
  // Helpers work like before but <this> is always the template instance!
  helpers: {
    page() {
      const { page } = this.props;
      return (page === 'disabled' && '...') || (page + 1);
    },
    currentPage() {
      const { page, currentPage } = this.props;
      return (page === 'disabled' && 'disabled') || (page === currentPage && 'active') || '';
    },
  },
  // Events work like before but <this> is always the template instance!
  events: {
    'click a'(e) {
      e.preventDefault();
      if (this.props.page !== 'disabled') {
        this.props.changePage();
      }
    },
  },
});

TemplateController('dataTablePaginate', {
  // Validate the properties passed to the template from parents
  props: new SimpleSchema({
    limit: Number,
    skip: Number,
    total: Number,
    onChange: Function,
  }),

  // Setup private reactive template state
  state: {},

  // Lifecycle callbacks work exactly like with standard Blaze
  onCreated() {
  },
  onRendered() {
  },
  onDestroyed() {
  },

  // Helpers work like before but <this> is always the template instance!
  helpers: {
    needPaginate() {
      return this.props.total > this.props.limit;
    },
    disabledPrev() {
      return (this.props.skip === 0 && 'disabled') || '';
    },
    disabledNext() {
      return (this.props.skip + this.props.limit >= this.props.total && 'disabled') || '';
    },
    listPages() {
      // считаем сколько нужно страниц
      const numberOfPages = Math.ceil(this.props.total / this.props.limit);
      // если не более 6 то выведем все 6 страниц
      if (numberOfPages <= 6) {
        return _.range(numberOfPages);
      }
      // если страниц больше 6 и мы в начале
      const page = this.props.skip / this.props.limit || 0;
      if (page < 3) {
        const begin = _.range(5);
        return [...begin, 'disabled', numberOfPages - 1];
      }
      const nextPages = page + 1;
      const prevPages = page - 1;
      if (nextPages < numberOfPages - 2) {
        const begin = _.range(prevPages, nextPages + 1);
        return [0, 'disabled', ...begin, 'disabled', numberOfPages - 1];
      }
      const end = _.range(prevPages, numberOfPages);
      return [0, 'disabled', ...end];
    },
    getPage(page) {
      const currentPage = (this.props.skip / this.props.limit || 0);
      return {
        page,
        currentPage,
        changePage: () => {
          this.props.onChange(this.props.limit * page);
        },
      };
    },
  },

  // Events work like before but <this.props> is always the template instance!
  events: {
    'click .js-paginate-prev'(e) {
      e.preventDefault();
      if (this.props.skip > 0) {
        this.props.onChange(this.props.skip - this.props.limit);
      }
    },
    'click .js-paginate-next'(e) {
      e.preventDefault();
      if (this.props.skip + this.props.limit < this.props.total) {
        this.props.onChange(this.props.skip + this.props.limit);
      }
    },
  },

  // These are added to the template instance but not exposed to the html
  private: {},
});
