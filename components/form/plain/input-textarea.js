import { TemplateController } from 'meteor/template-controller';
import SimpleSchema from 'simpl-schema';
import classnames from 'classnames';

// import template
import './input-textarea.html';

TemplateController('inputTextarea', {
  // Validate the properties passed to the template from parents
  props: new SimpleSchema({
    name: { type: String },
    className: { type: String, optional: true },
    id: { type: String, optional: true },
    placeholder: { type: String, optional: true },
    value: { type: String, optional: true },
    params: {
      type: new SimpleSchema({
        rows: { type: Number, defaultValue: 3 },
        cols: { type: Number, defaultValue: 30 },
      }),
      optional: true,
    },
  }),

  // Setup private reactive template state
  state: {},

  // Lifecycle callbacks work exactly like with standard Blaze
  onCreated() {
  },
  onRendered() {
  },
  onDestroyed() {
  },

  // Helpers work like before but <this> is always the template instance!
  helpers: {
    attributes() {
      const { name, className, id, params, placeholder } = this.props;
      return {
        name,
        class: classnames({ [className]: !!className, 'form-control': true }),
        id,
        placeholder,
        ...params,
      };
    },
  },

  // Events work like before but <this> is always the template instance!
  events: {},

  // These are added to the template instance but not exposed to the html
  private: {},
});
