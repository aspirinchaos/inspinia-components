import { TemplateController } from 'meteor/template-controller';
import SimpleSchema from 'simpl-schema';

// import template
import './input-text-number.html';

TemplateController('inputTextNumber', {
  // Validate the properties passed to the template from parents
  props: new SimpleSchema({
    name: { type: String },
    id: { type: String, optional: true },
    class: { type: String, optional: true },
    placeholder: { type: String, optional: true },
    value: { type: String, optional: true },
    onChange: { type: Function, optional: true },
    disabled: { type: Boolean, optional: true },
    maxlength: { type: Number, optional: true },
    minlength: { type: Number, optional: true },
    additional: { type: String, defaultValue: '' },
  }),

  // Setup private reactive template state
  state: {},

  // Lifecycle callbacks work exactly like with standard Blaze
  onCreated() {
    this.regexp = new RegExp(`[^0-9${this.props.additional}]+`);
  },
  onRendered() {
  },
  onDestroyed() {
  },

  // Helpers work like before but <this> is always the template instance!
  helpers: {
    attributes() {
      const atts = { ...this.data };

      delete atts.onChange;
      return {
        ...atts,
        pattern: `[0-9${this.props.additional}]*`,
        inputmode: 'numeric',
        class: this.props.class || 'form-control',
      };
    },
  },

  // Events work like before but <this> is always the template instance!
  events: {
    'input input'(e) {
      e.currentTarget.value = e.currentTarget.value.replace(this.regexp, '');
    },
    'change input'(e) {
      if (this.props.onChange) {
        this.props.onChange(e.currentTarget.value);
      }
    },
  },

  // These are added to the template instance but not exposed to the html
  private: {
    regexp: null,
  },
});
