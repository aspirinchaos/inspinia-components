import { TemplateController } from 'meteor/template-controller';
import SimpleSchema from 'simpl-schema';
import classNames from 'classnames';

// import template
import './button.html';

TemplateController('Button', {
  // Validate the properties passed to the template from parents
  props: new SimpleSchema({
    color: {
      type: String,
      defaultValue: 'default',
      allowedValues: ['default', 'primary', 'success', 'info', 'warning', 'danger', 'link', 'white'],
    },
    type: { type: String, defaultValue: 'button', allowedValues: ['button', 'submit'] },
    size: { type: String, optional: true, allowedValues: ['xs', 'sm', 'lg'] },
    content: { type: String, defaultValue: '' },
    className: { type: String, optional: true },
    onClick: { type: Function, optional: true },
    outline: { type: Boolean, defaultValue: false },
    disabled: { type: Boolean, defaultValue: false },
    block: { type: Boolean, defaultValue: false },
    rounded: { type: Boolean, defaultValue: false },
    circle: { type: Boolean, defaultValue: false },
    data: { type: Object, defaultValue: {}, blackbox: true },
  }),

  // Setup private reactive template state
  state: {},

  // Lifecycle callbacks work exactly like with standard Blaze
  onCreated() {
  },
  onRendered() {
  },
  onDestroyed() {
  },

  // Helpers work like before but <this> is always the template instance!
  helpers: {
    attributes() {
      const {
        type, className, color, size, outline, block, circle, rounded, disabled,
      } = this.props;
      return {
        class: classNames({
          [className]: !!className,
          [this.prefix]: true,
          [`${this.prefix}-${color}`]: !!color,
          [`${this.prefix}-${size}`]: !!size,
          [`${this.prefix}-${outline}`]: outline,
          [`${this.prefix}-${block}`]: block,
          [`${this.prefix}-${circle}`]: circle,
          [`${this.prefix}-${rounded}`]: rounded,
        }),
        type,
        disabled,
        ...this.formData(),
      };
    },
  },

  // Events work like before but <this> is always the template instance!
  events: {
    'click button'() {
      if (this.props.onClick) {
        this.props.onClick();
      }
    },
  },

  // These are added to the template instance but not exposed to the html
  private: {
    prefix: 'btn',
    // todo@aspirin сделать общим для всех компонентов?
    formData() {
      const { data } = this.props;
      const newData = {};
      Object.keys(data).forEach((key) => {
        newData[`data-${key}`] = data[key];
      });
      return newData;
    },
  },
});
