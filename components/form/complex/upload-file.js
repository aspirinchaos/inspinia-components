import { TemplateController } from 'meteor/template-controller';
import SimpleSchema from 'simpl-schema';

// import components
import '../plain/input-file';
import '../plain/input-hidden';

// import template
import './upload-file.html';

TemplateController('uploadFile', {
  // Validate the properties passed to the template from parents
  props: new SimpleSchema({
    name: { type: String, optional: true },
    id: { type: String, optional: true },
    accept: { type: String, defaultValue: 'image/*' },
    value: { type: String, optional: true },
    onChange: { type: Function, optional: true },
  }),

  // Setup private reactive template state
  state: {},

  // Lifecycle callbacks work exactly like with standard Blaze
  onCreated() {
  },
  onRendered() {
  },
  onDestroyed() {
  },

  // Helpers work like before but <this> is always the template instance!
  helpers: {
    buttonData() {
      return {
        color: 'primary',
        size: 'sm',
        onClick: () => {
          this.$('input[type=file]').trigger('click');
        },
        text: `<strong><i class="fa fa-upload"></i> ${(this.props.value && 'Изменить') || 'Выбрать'}</strong>`,
      };
    },
    attributesFile() {
      const { accept, id } = this.props;

      return {
        accept,
        id,
        className: 'upload-file__input',
        onChange: (files) => {
          if (files && files.length) {
            this.props.onChange(files[0]);
          }
        },

      };
    },
    attributesText() {
      const { name, value } = this.props;
      return { name, value };
    },

  },

  // Events work like before but <this> is always the template instance!
  events: {},

  // These are added to the template instance but not exposed to the html
  private: {
  },
});
