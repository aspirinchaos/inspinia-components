import { TemplateController } from 'meteor/template-controller';
import SimpleSchema from 'simpl-schema';

// import template
import './input-file.html';

TemplateController('inputFile', {
  // Validate the properties passed to the template from parents
  props: new SimpleSchema({
    multiple: { type: Boolean, defaultValue: false },
    accept: { type: String },
    id: { type: String, optional: true },
    className: { type: String, defaultValue: 'form-control' },
    onChange: { type: Function, optional: true },
  }),

  // Setup private reactive template state
  state: {},

  // Lifecycle callbacks work exactly like with standard Blaze
  onCreated() {
  },
  onRendered() {
  },
  onDestroyed() {
  },

  // Helpers work like before but <this> is always the template instance!
  helpers: {
    attributes() {
      const { multiple, accept, id, className } = this.props;
      return { multiple, accept, id, class: className };
    },
  },

  // Events work like before but <this> is always the template instance!
  events: {
    'change input'(e) {
      e.preventDefault();
      if (this.props.onChange) {
        this.props.onChange(e.currentTarget.files);
      }
    },
  },

  // These are added to the template instance but not exposed to the html
  private: {},
});
