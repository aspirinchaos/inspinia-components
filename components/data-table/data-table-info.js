import { TemplateController } from 'meteor/template-controller';
import SimpleSchema from 'simpl-schema';

import './data-table-info.html';

TemplateController('dataTableInfo', {
  // Validate the properties passed to the template from parents
  props: new SimpleSchema({
    limit: { type: Number },
    skip: { type: Number },
    total: { type: Number },
    selected: { type: Number },
  }),

  // Setup private reactive template state
  state: {},

  // Lifecycle callbacks work exactly like with standard Blaze
  onCreated() {
  },
  onRendered() {
  },
  onDestroyed() {
  },

  // Helpers work like before but <this> is always the template instance!
  helpers: {
    skip() {
      return this.props.skip + 1;
    },
    limit() {
      const limit = this.props.skip + this.props.limit;

      return limit < this.props.total ? limit : this.props.total;
    },
  },

  // Events work like before but <this> is always the template instance!
  events: {},

  // These are added to the template instance but not exposed to the html
  private: {},
});
