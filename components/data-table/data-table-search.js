import { TemplateController } from 'meteor/template-controller';
import SimpleSchema from 'simpl-schema';

import './data-table-search.html';

TemplateController('dataTableSearch', {
  // Validate the properties passed to the template from parents
  props: new SimpleSchema({
    search: { type: String, optional: true },
    onChange: Function,
  }),

  // Setup private reactive template state
  state: {},

  // Lifecycle callbacks work exactly like with standard Blaze
  onCreated() {
  },
  onRendered() {
  },
  onDestroyed() {
  },

  // Helpers work like before but <this> is always the template instance!
  helpers: {
    search() {
      return {
        name: 'search',
        value: this.props.search,
        onChange: this.props.onChange,
      };
    },
  },

  // Events work like before but <this> is always the template instance!
  events: {},

  // These are added to the template instance but not exposed to the html
  private: {},
});
