import { TemplateController } from 'meteor/template-controller';
import SimpleSchema from 'simpl-schema';

// import template
import './input-list.html';

TemplateController('InputList', {
  // Validate the properties passed to the template from parents
  props: new SimpleSchema({
    horizontal: { type: Boolean, defaultValue: false },
    hr: { type: Boolean, optional: true },
    name: { type: String },
    fields: { type: Array },
    'fields.$': {
      type: SimpleSchema.oneOf(String, {
        type: new SimpleSchema({
          template: { type: String, optional: true },
          field: { type: SimpleSchema.oneOf(String, Array) },
          'field.$': { type: String, optional: true },
          // параметры для поля
          params: { type: Object, blackbox: true, optional: true },
        }),
      }),
    },
    schema: {
      // слетела проверка типов для новой версии
      type: Object,
      blackbox: true,
      optional: true,
    },
    doc: { type: Object, blackbox: true, optional: true },
    size: { type: Object, blackbox: true, optional: true },
  }),

  // Setup private reactive template state
  state: {},

  // Lifecycle callbacks work exactly like with standard Blaze
  onCreated() {
  },
  onRendered() {
  },
  onDestroyed() {
  },

  // Helpers work like before but <this> is always the template instance!
  helpers: {
    schemaField(field) {
      // по дефолту
      let type = 'text';
      let params = {};
      // что в форме использовать что то посложней примитивов
      if (typeof field === 'object') {
        type = field.template || this.props.schema.getQuickTypeForKey(field.field);
        if (field.params && typeof field.params === 'object') {
          params = Object.assign({}, field.params);
        }
        field = field.field;
      } else {
        // если поле примитив
        type = this.props.schema.getQuickTypeForKey(field);
      }

      const { doc } = this.props;
      return {
        horizontal: this.props.horizontal,
        hr: this.props.hr,
        name: field,
        schemaName: this.props.name,
        label: this.label(field, params),
        type,
        value: (doc && this.getValue(doc, field)) || '',
        size: this.props.size,
        ...params,
      };
    },
  },

  // Events work like before but <this> is always the template instance!
  events: {},

  // These are added to the template instance but not exposed to the html
  private: {
    label(field, params) {
      if (params.label === false || (!params.label && !this.props.schema)) {
        delete params.label;
        return '';
      }
      return params.label || this.props.schema.label(field);
    },
    getValue(doc, field) {
      const [name, ...other] = field.split('.');
      if (!other.length) {
        return doc[name];
      }
      return this.getValue(doc[name], other.join('.'));
    },
  },
});
