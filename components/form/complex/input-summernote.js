import { TemplateController } from 'meteor/template-controller';
import SimpleSchema from 'simpl-schema';

// import template
import './input-summernote.html';

TemplateController('inputSummernote', {
  // Validate the properties passed to the template from parents
  props: new SimpleSchema({
    onChange: { type: Function },
    value: { type: String, optional: true },
    settings: { type: Object, blackbox: true, defaultValue: {} },
  }),

  // Setup private reactive template state
  state: {},

  // Lifecycle callbacks work exactly like with standard Blaze
  onCreated() {
  },
  onRendered() {
    const note = this.$('.summernote');

    note.summernote({
      lang: 'ru-RU',
      height: 600,
      ...this.props.settings,
    });

    this.props.onChange(() => note.summernote('code'));
  },
  onDestroyed() {
  },

  // Helpers work like before but <this> is always the template instance!
  helpers: {},

  // Events work like before but <this> is always the template instance!
  events: {},

  // These are added to the template instance but not exposed to the html
  private: {},
});
