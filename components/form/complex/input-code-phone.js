import { TemplateController } from 'meteor/template-controller';
import SimpleSchema from 'simpl-schema';

// import components
import '../plain/input-hidden';
import '../common/input-group';

// import template
import './input-code-phone.html';

TemplateController('inputCodePhone', {
  // Validate the properties passed to the template from parents
  props: new SimpleSchema({
    name: { type: String },
    id: { type: String, optional: true },
    value: { type: String, optional: true },
    groupClass: { type: String, optional: true },
  }),

  // Setup private reactive template state
  state: {
    code: null,
    phone: null,
  },

  // Lifecycle callbacks work exactly like with standard Blaze
  onCreated() {
    const { value } = this.props;
    // отсечем номер и оставим только код
    this.state.code = value ? value.slice(0, -10) : '+7';
    // получим только телефон
    this.state.phone = value ? value.slice(-10) : '';
  },
  onRendered() {
  },
  onDestroyed() {
  },

  // Helpers work like before but <this> is always the template instance!
  helpers: {
    groupData() {
      return {
        type: 'dropdown',
        className: this.props.groupClass,
        data: {
          text: this.state.code,
          handleSelect: (value) => {
            this.state.code = value;
          },
          options: ['+7', '+380', '+375'].map(value => ({ label: value, value })),
        },
      };
    },
    attributes() {
      return {
        id: this.props.id,
        placeholder: 'Телефон',
        maxlength: 10,
        value: this.state.phone,
        class: 'form-control',
      };
    },
    hiddenData() {
      return { name: this.props.name, value: `${this.state.code}${this.state.phone}` };
    },
  },

  // Events work like before but <this> is always the template instance!
  events: {
    'input input[type=tel]'(e) {
      e.currentTarget.value = e.currentTarget.value.replace(/([^0-9]+)/gi, '');
      this.state.phone = e.currentTarget.value;
    },
  },

  // These are added to the template instance but not exposed to the html
  private: {},
});
