import { TemplateController } from 'meteor/template-controller';
import SimpleSchema from 'simpl-schema';

// import components
import '../common/input-group';

// import template
import './input-clear.html';

/**
 * Readonly input с кнопкой для очищения поля
 */
TemplateController('inputClear', {
  // Validate the properties passed to the template from parents
  props: new SimpleSchema({
    name: { type: String, optional: true },
    id: { type: String, optional: true },
    className: { type: String, optional: true },
    placeholder: { type: String, optional: true },
    value: { type: String, optional: true },
  }),

  // Setup private reactive template state
  state: {
    value: '',
  },

  // Lifecycle callbacks work exactly like with standard Blaze
  onCreated() {
    this.autorun(() => {
      if (this.props.value) {
        this.state.value = this.props.value;
      }
    });
  },
  onRendered() {
  },
  onDestroyed() {
  },

  // Helpers work like before but <this> is always the template instance!
  helpers: {
    groupData() {
      return {
        type: 'btn-affix',
        className: this.props.groupClass,
        template: 'button',
        data: {
          color: 'primary',
          text: '<i class="fa fa-times-circle"></i>',
          onClick: () => {
            this.state.value = '';
          },
        },
      };
    },
    attributes() {
      const {
        name, id, className, placeholder,
      } = this.props;
      return {
        id,
        name,
        placeholder,
        value: this.state.value,
        readonly: true,
        class: `${className || ''} form-control`,
      };
    },
  },

  // Events work like before but <this> is always the template instance!
  events: {},

  // These are added to the template instance but not exposed to the html
  private: {},
});
