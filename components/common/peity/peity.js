import { TemplateController } from 'meteor/template-controller';
import SimpleSchema from 'simpl-schema';
import 'peity';

// import template
import './peity.html';

TemplateController('peity', {
  // Validate the properties passed to the template from parents
  props: new SimpleSchema({
    type: { type: String, allowedValues: ['pie', 'donut', 'line', 'bar'] },
    values: { type: SimpleSchema.oneOf(String, Array) },
    'values.$': { type: SimpleSchema.oneOf(Number, String), optional: true },
    fill: { type: SimpleSchema.oneOf(String, Array, Function) },
    'fill.$': { type: SimpleSchema.oneOf(Number, String), optional: true },
    width: { type: Number, optional: true },
    height: { type: Number, optional: true },
    stroke: { type: String, optional: true },
    strokeWidth: { type: Number, optional: true },
  }),

  // Setup private reactive template state
  state: {},

  // Lifecycle callbacks work exactly like with standard Blaze
  onCreated() {
  },
  onRendered() {
    const { type, fill, width, height, stroke, strokeWidth } = this.props;
    this.$('span').peity(type, {
      fill,
      width,
      height,
      stroke,
      strokeWidth,
    });
  },
  onDestroyed() {
  },

  // Helpers work like before but <this> is always the template instance!
  helpers: {
    values() {
      const { values } = this.props;
      return (Array.isArray(values) && values.join(',')) || values;
    },
  },

  // Events work like before but <this> is always the template instance!
  events: {},

  // These are added to the template instance but not exposed to the html
  private: {},
});
