import { TemplateController } from 'meteor/template-controller';
import SimpleSchema from 'simpl-schema';

// import template
import './button-dropdown-element.html';

TemplateController('buttonDropdownElement', {
  // Validate the properties passed to the template from parents
  props: new SimpleSchema({
    value: { type: String, optional: true },
    label: { type: String, optional: true },
    doc: { type: String, optional: true },
    handleSelect: { type: Function, optional: true },
  }),

  // Setup private reactive template state
  state: {},

  // Lifecycle callbacks work exactly like with standard Blaze
  onCreated() {
  },
  onRendered() {
  },
  onDestroyed() {
  },

  // Helpers work like before but <this> is always the template instance!
  helpers: {},

  // Events work like before but <this> is always the template instance!
  events: {
    'click a'(e) {
      e.preventDefault();
      if (this.props.handleSelect) {
        const { value, doc } = this.props;
        this.props.handleSelect(value, doc);
      }
    },
  },

  // These are added to the template instance but not exposed to the html
  private: {},
});
