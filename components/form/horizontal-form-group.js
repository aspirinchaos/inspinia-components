import { TemplateController } from 'meteor/template-controller';
import SimpleSchema from 'simpl-schema';
import classNames from 'classnames';

// import components
import '../common/hr-line-dashed';

// import template
import './horizontal-form-group.html';

TemplateController('HorizontalFormGroup', {
    // Validate the properties passed to the template from parents
  props: new SimpleSchema({
    checkbox: { type: Boolean, defaultValue: false },
    hr: { type: Boolean, defaultValue: true },
    label: { type: String, optional: true },
    className: { type: String, optional: true },
    id: { type: String, optional: true },
    size: {
      type: new SimpleSchema({
        lg: { type: Number, min: 1, max: 12, defaultValue: 10 },
        md: { type: Number, min: 1, max: 12, defaultValue: 9 },
        sm: { type: Number, optional: true, min: 1, max: 12 },
        xs: { type: Number, optional: true, min: 1, max: 12 },
      }),
      defaultValue: {},
    },
    help: { type: String, optional: true },
  }),

    // Setup private reactive template state
  state: {},

    // Lifecycle callbacks work exactly like with standard Blaze
  onCreated() {
  },
  onRendered() {
  },
  onDestroyed() {
  },

    // Helpers work like before but <this> is always the template instance!
  helpers: {
    labelSize() {
      const { lg, md, sm, xs } = this.props.size;
      return classNames({
        'control-label': true,
        [`col-lg-${12 - lg}`]: !!lg,
        [`col-md-${12 - md}`]: !!md,
        [`col-sm-${12 - sm}`]: !!sm,
        [`col-xs-${12 - xs}`]: !!xs,
      });
    },
    inputSize() {
      const { lg, md, sm, xs } = this.props.size;
      return classNames({
        [`col-lg-${lg}`]: !!lg,
        [`col-md-${md}`]: !!md,
        [`col-sm-${sm}`]: !!sm,
        [`col-xs-${xs}`]: !!xs,
      });
    },
  },

    // Events work like before but <this> is always the template instance!
  events: {},

    // These are added to the template instance but not exposed to the html
  private: {},
});
