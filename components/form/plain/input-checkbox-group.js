import { TemplateController } from 'meteor/template-controller';
import SimpleSchema from 'simpl-schema';

// import components
import './input-checkbox';

// import template
import './input-checkbox-group.html';

TemplateController('inputCheckboxGroup', {
  // Validate the properties passed to the template from parents
  props: new SimpleSchema({
    name: { type: String },
    class: { type: String, optional: true },
    id: { type: String },
    onChange: { type: Function, optional: true },
    value: { type: Array, optional: true },
    'value.$': { type: String },
    options: { type: Array },
    'options.$': {
      type: new SimpleSchema({
        value: { type: String },
        label: { type: String },
      }),
    },
  }),

  // Setup private reactive template state
  state: {},

  // Lifecycle callbacks work exactly like with standard Blaze
  onCreated() {
  },
  onRendered() {
  },
  onDestroyed() {
  },

  // Helpers work like before but <this> is always the template instance!
  helpers: {
    field(option, index) {
      const atts = { ...this.data };

      delete atts.options;
      delete atts.value;
      return {
        ...atts,
        ...option,
        id: `${this.props.id}-${index}`,
        checked: this.props.value.indexOf(option.value) !== -1,
      };
    },
  },

  // Events work like before but <this> is always the template instance!
  events: {},

  // These are added to the template instance but not exposed to the html
  private: {},
});
