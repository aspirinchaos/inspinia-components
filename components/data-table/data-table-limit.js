import { TemplateController } from 'meteor/template-controller';
import SimpleSchema from 'simpl-schema';

import './data-table-limit.html';

TemplateController('dataTableLimit', {
  // Validate the properties passed to the template from parents
  props: new SimpleSchema({
    limit: Number,
    onChange: Function,
  }),

  // Setup private reactive template state
  state: {},

  // Lifecycle callbacks work exactly like with standard Blaze
  onCreated() {
  },
  onRendered() {
  },
  onDestroyed() {
  },

  // Helpers work like before but <this> is always the template instance!
  helpers: {
    length() {
      return {
        name: 'length',
        options: [10, 25, 50, 100].map(i => ({ value: i, label: i })),
        value: this.props.limit,
        onChange: value => this.props.onChange(Number(value)),
      };
    },
  },

  // Events work like before but <this> is always the template instance!
  events: {},

  // These are added to the template instance but not exposed to the html
  private: {},
});
