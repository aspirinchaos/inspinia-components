import { TemplateController } from 'meteor/template-controller';
import SimpleSchema from 'simpl-schema';
import moment from 'moment';
// import

// import template
import './input-date.html';


TemplateController('inputDate', {
  props: new SimpleSchema({
    name: { type: String },
    class: { type: String },
    id: { type: String },
    // для пустой строки
    value: { type: SimpleSchema.oneOf(String, Date), optional: true },
  }),
  // Setup private reactive template state
  state: {},

  // Lifecycle callbacks work exactly like with standard Blaze
  onCreated() {
  },
  onRendered() {
  },
  onDestroyed() {
  },

  // Helpers work like before but <this> is always the template instance!
  helpers: {
    attributes() {
      return {
        ...this.data,
        value: moment(this.data.value).format('DD.MM.YYYY'),
      };
    },
  },

  // Events work like before but <this> is always the template instance!
  events: {},

  // These are added to the template instance but not exposed to the html
  private: {},
});
