import { TemplateController } from 'meteor/template-controller';
import { Random } from 'meteor/random';
import SimpleSchema from 'simpl-schema';

// import components
import './horizontal-form-group';
import './vertical-form-group';
// import plain
import './plain/input-text';
import './plain/input-password';
import './plain/input-number';
import './plain/input-date';
import './plain/input-phone';
import './plain/input-select';
import './plain/input-textarea';
import './plain/input-checkbox';
import './plain/input-checkbox-group';
import './plain/input-email';
import './plain/input-radio-group';
// import complex
import './complex/date-picker';
import './complex/date-range';
import './complex/clock-picker';
import './complex/input-code-phone';
import './complex/input-text-number';
import './complex/double-input';
import './complex/input-summernote';
import './complex/upload-file';
import './complex/color-picker';
import './complex/input-copy';
import './complex/input-clear';
// import template
import './input.html';

const primitiveInputs = {
  textarea: 'inputTextarea',
  email: 'inputEmail',
  phone: 'inputPhone',
  codePhone: 'inputCodePhone',
  string: 'InputText',
  text: 'InputText',
  password: 'inputPassword',
  textNumber: 'inputTextNumber',
  number: 'inputNumber',
  date: 'datePicker',
  time: 'clockPicker',
  boolean: 'InputCheckbox',
  select: 'inputSelect',
  double: 'doubleInput',
  summernote: 'inputSummernote',
  file: 'uploadFile',
  color: 'colorPicker',
  checkboxGroup: 'inputCheckboxGroup',
  copy: 'inputCopy',
  clear: 'inputClear',
};

TemplateController('Input', {
  // Validate the properties passed to the template from parents
  // props: new SimpleSchema({}),

  // Setup private reactive template state
  state: {},

  // Lifecycle callbacks work exactly like with standard Blaze
  onCreated() {
    this.autorun(() => {
      new SimpleSchema({
        horizontal: { type: Boolean, defaultValue: false, optional: true },
        hr: { type: Boolean, optional: true },
        name: { type: SimpleSchema.oneOf(String, Array), optional: true },
        'name.$': { type: String, optional: true },
        schemaName: {
          type: String,
          optional: true,
          autoValue() {
            if (!this.isSet) {
              return Random.id();
            }
          },
        },
        label: { type: String, optional: true },
        type: { type: String },
        inputClassName: { type: String, optional: true },
        groupClassName: { type: String, optional: true },
        placeholder: { type: String, optional: true },
        maxlength: { type: Number, optional: true },
        minlength: { type: Number, optional: true },
        // todo@aspirin Последовательность типов очень важна! схема пытается сравнить типы по
        // очереди и возвращает 1 подходящий. у объекта Date есть функция для превращение даты в
        // строку, поэтому такое и работает. oneOf меняет тип только по 1 параметру (mutate throw
        // props)
        value: { type: SimpleSchema.oneOf(Date, Number, Boolean, String, Array), optional: true },
        'value.$': { type: SimpleSchema.oneOf(Number, String), optional: true },
        onClick: { type: Function, optional: true },
        onChange: { type: Function, optional: true },
        onInput: { type: Function, optional: true },
        disabled: { type: Boolean, optional: true },
        readonly: { type: Boolean, optional: true },
        size: { type: Object, blackbox: true, optional: true },
        help: { type: String, optional: true },
        // number
        step: { type: Number, optional: true },
        min: { type: Number, optional: true },
        max: { type: Number, optional: true },
        integer: { type: Boolean, optional: true },
        // date
        startDate: { type: Date, optional: true },
        clear: { type: Boolean, optional: true },
        // select
        options: { type: Array, optional: true },
        'options.$': {
          type: SimpleSchema.oneOf(new SimpleSchema({
            value: { type: SimpleSchema.oneOf(String, Number), optional: true },
            label: { type: SimpleSchema.oneOf(String, Number) },
            disabled: { type: Boolean, optional: true },
          }), String, Number),
        },
        multiple: { type: Boolean, defaultValue: false, optional: true },
        tags: { type: Boolean, defaultValue: false, optional: true },
        selected: { type: SimpleSchema.oneOf(String, Array), optional: true },
        'selected.$': { type: String, optional: true },
        // summernote
        settings: { type: Object, blackbox: true, optional: true },
        // double
        onlyNumbers: { type: Boolean, defaultValue: false, optional: true },
        // file
        accept: { type: String, optional: true },
      }).validate(Template.currentData());
    });
  },
  onRendered() {
  },
  onDestroyed() {
  },

  // Helpers work like before but <this> is always the template instance!
  helpers: {
    formGroupData() {
      const { type, schemaName, hr, name, label, size, groupClassName: className, help } = this.data;
      return {
        // используется при одиночном чекбоксе
        checkbox: type === 'boolean',
        id: `${schemaName}-${name}`,
        label,
        size,
        className,
        help,
        hr,
      };
    },
    isHorizontal() {
      return this.data.horizontal;
    },
    template() {
      const { type } = this.data;

      return primitiveInputs[type] || type;
    },
    attributes() {
      // todo переписать на props
      const atts = { ...this.data };
      atts.id = `${this.data.schemaName}-${this.data.name}`;
      if (atts.inputClassName) {
        atts.className = atts.inputClassName;
        delete atts.inputClassName;
      }

      delete atts.groupClassName;
      delete atts.schemaName;
      delete atts.label;
      delete atts.horizontal;
      delete atts.type;
      delete atts.hr;
      delete atts.size;

      return atts;
    },
    checkboxData() {
      const atts = { ...this.data };

      atts.checked = !!atts.value || false;
      atts.value = true;
      atts.id = `${this.data.schemaName}-${this.data.name}`;

      if (atts.inputClassName) {
        atts.className = atts.inputClassName;
        delete atts.inputClassName;
      }

      delete atts.groupClassName;
      delete atts.schemaName;
      delete atts.type;
      delete atts.hr;
      delete atts.horizontal;

      return atts;
    },
  },

  // Events work like before but <this> is always the template instance!
  events: {},

  // These are added to the template instance but not exposed to the html
  private: {},
});
