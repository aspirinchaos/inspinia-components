import { TemplateController } from 'meteor/template-controller';
import SimpleSchema from 'simpl-schema';

// import template
import './input-phone.html';

TemplateController('inputPhone', {
  // Validate the properties passed to the template from parents
  props: new SimpleSchema({
    name: { type: String },
    id: { type: String },
    className: { type: String, optional: true },
    value: { type: String, optional: true },
  }),

  // Setup private reactive template state
  state: {},

  // Lifecycle callbacks work exactly like with standard Blaze
  onCreated() {
  },
  onRendered() {
  },
  onDestroyed() {
  },

  // Helpers work like before but <this> is always the template instance!
  helpers: {
    attributes() {
      const atts = { ...this.data };

      return {
        ...atts,
        class: this.className || 'form-control',
      };
    },
  },

  // Events work like before but <this> is always the template instance!
  events: {
    'input input'(e) {
      const value = e.currentTarget.value;
      if (!SimpleSchema.RegEx.Phone.test(value)) {
        e.currentTarget.value = value.replace(/([^+0-9]+)/gi, '');
      }
    },
  },

  // These are added to the template instance but not exposed to the html
  private: {},
});
